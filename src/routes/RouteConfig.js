import React from "react";
import Login from "../pages/Login";
import RouteMiddleware from "./RouteMiddleware";
import Home from "../pages/Home";
import FavoritesList from "../pages/Favorites";
import DetailPokemon from "../pages/DetailPokemon";

export default  [
    {
      name:"Login",  
      path: "/",
      component: Login,
      exact:true,
      routes:[]
    },
     {
      path: "/logged",
      component: RouteMiddleware,
      exact:false,
      name:"loggedroures",
      routes: [
        {
          name:"home",
          path: "/logged/home",
          component: Home,
          exact:false,
        },
        {
          name:"favorites",
          path: "/logged/favorites",
          component: FavoritesList,
          exact:false,
        },
         {
          name:"detail",
          path: "/logged/detailPokemon/:pokemonId",
          component: DetailPokemon,
          exact:false,
        } 
      ]
    }
  ];