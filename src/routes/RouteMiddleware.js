import React,{useState} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";

  import { Result, Button } from 'antd';
import {useHistory} from "react-router-dom";
import {useSesionValidator} from "../hooks/useSesionValidator";
import TabNavigation from "../components/template/TabNavigation";

  export default function RouteMiddleware(props){
    
    let isLogged  = useSesionValidator();
    let history = useHistory();
    let [tabIndex,setTabIndex] = useState(-1);







    const SelectTab = (tabIndex,path)=>{
      setTabIndex(tabIndex);
     
      history.push(path);

    }


    return (

        <React.Fragment>
        {isLogged?     <React.Fragment>
            <Switch>
              {
                  props.routes.map(ele=>
                      <Route
                      key={ele.name}
                      //exact={ele.exact}
                      path={ele.path}
                      render={ props=> <ele.component {...props}   />}
                      />

                    
                      )
              }
          </Switch>
          <TabNavigation
          tabSelected={tabIndex}
          setTabIndex={SelectTab}

          />

        </React.Fragment> :
       <Result
       status="404"
       title="Sesion no valida"
       subTitle="Para ingresar a la pagina web debe iniciar sesion"
       extra={<Button onClick={evt=>history.push("/")}  type="primary">Ir al login</Button>}
     />
       }
      </React.Fragment>

      
    )

  }