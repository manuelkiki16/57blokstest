import Identifiers  from "./Identifiers";
import {LOCA_STORAGE_USER_FAVORITE} from "../utils/constants";

const InitialState = {
    isLogged:false,
    loggedUser:{
        UUID:"",
        Correo:"",
        Clave:""
    },
    masterDataList:[],
    dataList:[],
    detailObject:{},
    favorites:[],
    tabIndex:0
}


export const  AppReducer = (state=InitialState,action)=>{

    let newState = {...state};

    switch (action.type) {
        case Identifiers.IS_LOGGED:
            newState.isLogged = !newState.isLogged;
        break;

        case Identifiers.RESET_STATE:
            newState = InitialState;
        break;
        
        case Identifiers.RESET_STATE:
            newState = InitialState;
        break;
        case Identifiers.SET_MASTET_DATA_LIST:
            newState.masterDataList = action.payload;
        break;
        case Identifiers.REPLACE_FAVORITES:
            newState.favorites = action.payload;
            localStorage.setItem(LOCA_STORAGE_USER_FAVORITE,JSON.stringify(newState.favorites));
        break;
        case Identifiers.ADD_FAVORITE:
            newState.favorites.push(action.payload);
            localStorage.setItem(LOCA_STORAGE_USER_FAVORITE,JSON.stringify(newState.favorites));
        break;
        case Identifiers.SET_DATA_LIST:
            newState.dataList = action.payload;
        break;
        case Identifiers.SET_DATA_LOGGED_USER:
            newState.loggedUser = action.payload;
        break;
        case Identifiers.REMOVE_FAVORITE:
            newState.favorites = newState.favorites.filter( ele=>ele.id !== action.payload );
            localStorage.setItem(LOCA_STORAGE_USER_FAVORITE,JSON.stringify(newState.favorites));
        break;

    
        default:
            break;
    }


    return newState;


}

