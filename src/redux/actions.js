import Identifiers from "./Identifiers";

/**
 * Change login status in redux
 * @returns Reducers data
 */
export const ChangeLogState = ()=>{

return {
    type:Identifiers.IS_LOGGED,
    payload:null
}

}
export const ResetState = ()=>{
  
    return {
        type:Identifiers.RESET_STATE,
        payload:null
    }
    
}
export const SetMasterDataList = (list) => {
    return {
        type:Identifiers.SET_MASTET_DATA_LIST,
        payload:list
    }

}

export const SetDataList = (list) => {
    return {
        type:Identifiers.SET_DATA_LIST,
        payload:list
    }

}
export const ReplaceFavorites = (listFav) => {
    return {
        type:Identifiers.REPLACE_FAVORITES,
        payload:listFav
    }

}
export const AddFavorite = (favorite) => {
    return {
        type:Identifiers.ADD_FAVORITE,
        payload:favorite
    }

}
export const RemoveFavorite = (favorite) => {
    return {
        type:Identifiers.REMOVE_FAVORITE,
        payload:favorite
    }

}
export const SetDataLoggedUser = (user)=>{

    return {
        type:Identifiers.SET_DATA_LOGGED_USER,
        payload:user
    }  

}