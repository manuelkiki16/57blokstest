import React from "react";
import { CSSTransition } from 'react-transition-group';
import PropTypes from "prop-types";



const FadeTransition = (props)=>{

return (
    <CSSTransition
     classNames='custom-fade'
     timeout={props.timeOut? props.timeOut : 500}
     unmountOnExit={true}
     in={props.isEnabled}

    >
          {props.render}
    </CSSTransition>

)

}
const BouncTransition = (props)=>{

    return (
        <CSSTransition
         classNames='custom-bounc'
         timeout={props.timeOut? props.timeOut : 500}
         unmountOnExit={true}
         in={props.isEnabled}
    
        >
              {props.render}
        </CSSTransition>
    
    )
    
    }

FadeTransition.propTypes= {
    render:PropTypes.any.isRequired,
    isEnabled:PropTypes.bool.isRequired,
    timeOut: PropTypes.number,


}
BouncTransition.propTypes= {
    render:PropTypes.any.isRequired,
    isEnabled:PropTypes.bool.isRequired,
    timeOut: PropTypes.number,


}


export {
    FadeTransition,
    BouncTransition
}