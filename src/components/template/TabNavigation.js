import React from "react";
import { Result, Button,Typography } from 'antd';
import {useHistory} from "react-router-dom";
import {Row,Col,Container} from "react-grid-system";
import {HomeOutlined,HeartOutlined} from "@ant-design/icons";


export default (props)=>{

  let history = useHistory();


    return (
        <div className='fixed-tab' >
       <Row>
            <Col sm={6} xs={6} md={6}  >
              <div 
              onClick={evt=>props.setTabIndex(0,'/logged/home')} 
              className={ props.tabSelected == 0 || history.location.pathname === '/logged/home' ?'custom-tab-btn custom-tab-btn-selected':'custom-tab-btn '  } >
                 <div className='custom-tab-ic-container' >
                    <HomeOutlined />
                 </div>
                  <label>HOME </label>
                 
              </div>
            </Col>
            <Col sm={6} xs={6} md={6} >
            <div 
            onClick={evt=>props.setTabIndex(1,'/logged/favorites')}  
            className={ props.tabSelected == 1 || history.location.pathname === '/logged/favorites'  ?'custom-tab-btn custom-tab-btn-selected':'custom-tab-btn '}  >
              <div className='custom-tab-ic-container' >
                <HeartOutlined />
              </div>

              <label>FAVORITE </label>

            </div>
            </Col>
            <Col>
            </Col>
          </Row>

      </div>
    )


}