import React from "react";
import  {Row,Col} from "react-grid-system";
import {Input} from "antd";
 
export default (props) => {


    return (
        <div className='header-content' >


            <Row>
                <Col sm={2} md={4} >

                </Col>
                <Col sm={8} md={4} >
                    <div className='search-container'    >
                        <div className='button-left' >
                            Buscar
                        </div>
                        <Input onChange={ evt=>props.setFilterText(evt.target.value) } className='input-search' placeholder='Nombre del pokemon' />
                      
                    </div>
                </Col>
                <Col  sm={2} md={4} >

                </Col>

            </Row>

        </div>
    )

}