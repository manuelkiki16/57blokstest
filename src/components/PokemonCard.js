import React from "react";
import { Card,Button,Tag} from "antd";
import Pokemon from "../assets/img/defalt_test_pokemon.png";
import {Row,Col,Container,} from "react-grid-system";
import  PropTypes from "prop-types";
import {HeartFilled,HeartOutlined} from "@ant-design/icons";


function PokemonCard(props){



return (
   <Card   className={`no-border-sizing card-list-pk  ${ props.marked?'item-marked':'' }  `} >
       
       <Container>
        <Row>
        <Col sm={12} md={12} >
            <div className='favorite-container' >
                {
                    props.is_in_favorite?
                    <HeartFilled  onClick={props.remove_to_favorite}  className='favorite-icon' />
                    :
                    <HeartOutlined  onClick={props.add_to_favorite}  className='favorite-icon' />
                }
               
            </div>

        </Col>
            <Col sm={12} md={12} >
                <div className='card-image-content-list'  >
                    <img src={props.pokemon_image} className='pokemon-img-list'  />
                </div>
            </Col>
            <Col sm={12} md={12} >
                <p className='card-pokemon-name' >{props.pokemon_name}</p>
               {props.marked &&  <Tag color='red' >Elemento visible</Tag>}
            </Col>
            <Col sm={6} xs={6} md={4} >
                <p className='card-pokemon-detail'  >P. Exp: {props.pokemon_exp}</p>
            </Col>
            <Col sm={6} xs={6} md={4} >
            <p className='card-pokemon-detail'  >Peso: {props.pokemon_weight}</p>
            </Col>
            <Col sm={6} xs={6} md={4} >
            <p className='card-pokemon-detail'  >Altura: {props.pokemon_height}</p>
            </Col>
            <Col sm={12} md={12} >
                <Button  onClick={props.go_to_detail}  block={true} type="primary" size="middle"  >Ver detalles</Button>
            </Col>
       
        </Row>
       </Container>
       
   </Card>
)

}
PokemonCard.propTypes= {
go_to_detail:PropTypes.func.isRequired,   
add_to_favorite:PropTypes.func.isRequired, 
remove_to_favorite:PropTypes.func.isRequired, 
is_in_favorite:PropTypes.bool,
pokemon_name:PropTypes.string.isRequired,
pokemon_exp:PropTypes.number.isRequired,
pokemon_weight:PropTypes.number.isRequired,
pokemon_height:PropTypes.number.isRequired,
pokemon_image:PropTypes.string.isRequired,


}

export default PokemonCard;