import React from "react";
import { Typography, Tag, Button } from "antd";
import { Row, Col,Visible } from "react-grid-system";
import PokemondDefault from "../assets/img/defalt_test_pokemon.png";
import { DeleteOutlined } from "@ant-design/icons";
import PropTypes from "prop-types"; 

 function FavoritesItem(props) {


    return (
        <div className='custom-item-fab' >
            <Row align="center"  >
                <Col sm={12} xs={12} md={4} >
                    <div onClick={props.go_to_detail}  className='custom-item-fab-img' >
                    <img src={props.favorite_image} className='img-fav' />
                    </div>
                </Col>
                <Col sm={12} xs={12} md={6} >
                    <div onClick={props.go_to_detail}  className='detail-fab' >
                        <Typography.Title level={5} >{props.favorite_name}</Typography.Title>
                        <Tag color="warning" >Nivel de experiencia {props.favorite_ext}</Tag>
                    </div>
                </Col>
                <Col sm={12} xs={12} md={2} >

                   <div className='btn-delete-content' >
                  <Visible md xxl xl   >
                    <Button onClick={props.drop_favorite}  danger type="primary" icon={<DeleteOutlined />} size="middle" shape="circle" ></Button>
                  </Visible>
                  <Visible sm xs  >
                   <div style={{marginTop:10}}>
                   <Button onClick={props.drop_favorite}  danger type="primary" icon={<DeleteOutlined />} size="large" block >Eliminar</Button>
                   </div>
                  </Visible>
                   </div>

                </Col>

            </Row>
        </div>
    )

}

FavoritesItem.propTypes = {
    favorite_image:PropTypes.string.isRequired,
    favorite_name:PropTypes.string.isRequired,
    favorite_ext:PropTypes.number.isRequired,
    drop_favorite:PropTypes.func.isRequired,
    go_to_detail:PropTypes.func


}
export default FavoritesItem;