import React,{useEffect,useState} from "react";
import {useHistory} from "react-router-dom";

import {useSelector,useDispatch} from "react-redux";
import {GetSesionData,getFavorites} from "../utils/global-methods";
import {ChangeLogState,SetDataLoggedUser,ReplaceFavorites} from "../redux/actions";


export const useSesionValidator  = ()=>{

   let [hasSesion,setHasSesion] = useState(false);
   let history = useHistory();
   let dispatch = useDispatch();
   let isLogged  = useSelector( state=>state.isLogged );


   useEffect( ()=>{


     if( history.location.pathname === '/' && isLogged === false  ){

        let sesionData = GetSesionData();
        if( sesionData ){
            dispatch(ReplaceFavorites( getFavorites(sesionData.UUID)  ));
            dispatch( ChangeLogState() );
            dispatch( SetDataLoggedUser(sesionData) );
          
            setHasSesion(true);
        }else{
            setHasSesion(false); 
        }

     }else if( isLogged === true ) {
        setHasSesion(true);
     }



   },[history.location,isLogged]);
   useEffect(()=>{

    if( isLogged === false  ){

        let sesionData = GetSesionData();
        if( sesionData ){
            dispatch(ReplaceFavorites( getFavorites(sesionData.UUID)  ));
            dispatch( ChangeLogState() );
            dispatch( SetDataLoggedUser(sesionData) );
            //history.push("/logged/home");
            setHasSesion(true);
        }else{
            setHasSesion(false); 
        }

    }


   },[]);


   
   return hasSesion;

}