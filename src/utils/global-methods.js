import {LOCAL_STORAGE_USERS_DATA,LOCAL_STORAGE_SESION,LOCA_STORAGE_USER_FAVORITE} from "./constants";
import {v4 as UUIDV4} from "uuid";
/**
 * 
 * @param {*} user 
 * this method admin the users data in local storage
 */
export  function SaveSesion(user){

    let result = localStorage.getItem(LOCAL_STORAGE_USERS_DATA);
    let statusSesion = {msg:"",state:true,user:{}};
    

    if( result  ){

        result = JSON.parse(result);

        let findIndex = result.findIndex( ele=>String(ele.Correo).toLowerCase() === String(user.Correo).toLowerCase() );

        if( findIndex != -1 ){

            if( result[findIndex].Clave === user.Clave ){

                localStorage.setItem(LOCAL_STORAGE_SESION,JSON.stringify(result[findIndex]));
                statusSesion.user = result[findIndex];
            }else{
                statusSesion = {msg:"La clave es incorrecta",state:false};
            }

        }else{
            user.UUID = UUIDV4();
            result.push(user);
            statusSesion.user = user;
            localStorage.setItem(LOCAL_STORAGE_USERS_DATA,JSON.stringify(result));
            localStorage.setItem(LOCAL_STORAGE_SESION,JSON.stringify(user));
        }


    }else{
        user.UUID = UUIDV4();
        statusSesion.user = user;
        localStorage.setItem(LOCAL_STORAGE_USERS_DATA,JSON.stringify([user]));
        localStorage.setItem(LOCAL_STORAGE_SESION,JSON.stringify(user));

    }

    return statusSesion;


}
export const GetSesionData = ()=>{

    let sesion = localStorage.getItem(LOCAL_STORAGE_SESION);
    if( !sesion ){
        return null;
    }
    return JSON.parse(sesion);

}

export const getFavorites =(UUID)=>{

    let favData = localStorage.getItem(LOCA_STORAGE_USER_FAVORITE);
    if( !favData ){
        return [];
    }

    return JSON.parse(favData);
    


}
export const IsInFavorite = (pkId,listFav=[])=>{
    
    return listFav.findIndex( ele=>ele.id == pkId ) !== -1;


}
