import Axios from "axios";
import {BASE_URL} from "../utils/constants";

export const PokeRequest = Axios.create({
    baseURL:BASE_URL,
    timeout:10000
});

/**
 * other axios instances
 */