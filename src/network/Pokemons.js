import {PokeRequest} from "./AxiosConfig";
import {LIMIT_POKEMON_QUERY} from "../utils/constants";


export const GetPokemonList = (path='pokemon?offset=0&limit='+LIMIT_POKEMON_QUERY)=>{

return PokeRequest.get(path);


}

export const GetPokemonDetail = (pokemonId)=>{

    return PokeRequest.get(`pokemon/${pokemonId}`);
    
}

