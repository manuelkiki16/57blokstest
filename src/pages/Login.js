import React from "react";
import {Row,Col,Container} from "react-grid-system";
import { Input,Form,Button,notification } from 'antd';
import {SaveSesion} from "../utils/global-methods";
import {ChangeLogState,SetDataLoggedUser} from "../redux/actions";
import {useDispatch} from "react-redux";
import {useHistory,Redirect} from "react-router-dom";
import {useSesionValidator}  from "../hooks/useSesionValidator";

export default function Login(props){

    let dispatch = useDispatch();
    let history = useHistory();
    let isLogged = useSesionValidator();
    const validateMessages = {
        required: 'El campo ${label} es requerido!',
        types: {
          email: 'El campo ${label} no es un email valid!',
        },
        string: {
            min: "El campo ${label} debe ser al menos de  ${min} caracteres",
          },
      };


      const onSendUser = (values) => {

      
        let saveState = SaveSesion(values.user);

        if( !saveState.state ){
            notification.error({
                message:"Inicio de sesion no valido",
                description:saveState.msg
            });
        }else{
            notification.success({
                message:"Bienvenido",
                description:"Bienvenido usuario",
                placement:"topRight",
                duration:1000
        });

        dispatch( ChangeLogState() );
        dispatch( SetDataLoggedUser(saveState.user) );
        setTimeout(()=>{
            history.push("/logged/home");

        },1500);
      
        

        }
      };

/*       useEffect(() => {

        let sesionData = GetSesionData();

        if( sesionData ){
            dispatch(ReplaceFavorites( getFavorites(sesionData.UUID)  ));
            dispatch( ChangeLogState() );
            dispatch( SetDataLoggedUser(sesionData) );
            history.push("/logged/home");
        }
         
      }, []); */


return (
    <div className='login-container' >
        {
           isLogged === true && <Redirect to='/logged/home' />
         }
        <Container>
            <Row>
                <Col sm={12} md={8} >

                </Col>
                <Col sm={12} md={4} >

                    <div className='login-form-container' >
                        <div className='login-form-wrapper' >
                            <h1>Inicio de sesion</h1>
                            <Form
                                name="nest-messages"
                                layout="vertical"
                                onFinish={onSendUser}
                                validateMessages={validateMessages}
                            >
                                <Form.Item name={['user', 'Correo']} label="Correo" rules={[{ required: true,type:"email" }]}   >
                                    <Input placeholder='Hola mundo' />
                                </Form.Item>
                                <Form.Item name={['user', 'Clave']} label="Contraseña" rules={[{ required: true,min:8 }]} >
                                    <Input type='password' placeholder='Hola mundo' />
                                </Form.Item>
                                <Form.Item>
                                    <Button block={true} type="primary" htmlType="submit" size="large"  >Iniciar sesion</Button>
                                </Form.Item>

                            </Form>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>

       

    </div>
)

}