import React,{useState,useEffect} from "react";
import {Card,Tag,Typography,Result,Button,List} from "antd";
import {Container,Row,Col} from "react-grid-system";
import PokemonDefault from "../assets/img/defalt_test_pokemon.png";
import {StarOutlined} from "@ant-design/icons";
import {useParams,useHistory} from "react-router-dom";
import {GetPokemonDetail} from "../network/Pokemons";
import { FadeTransition, BouncTransition} from "../components/animations/CustomAnimate";



export default function DetailPokemon(props){

    let {pokemonId} = useParams();
    let history = useHistory();
    let [pokemonData,setPokemonData] = useState({});
    let [errorPage,setErrorPage] = useState(false);
    let [enableTransition,setEnableTransition] = useState(false);
   

    useEffect(()=>{

        GetPokemonDetail(pokemonId).then(result=>{
            setPokemonData(result.data);
            setEnableTransition(true);
        }).catch(err=>{
            setErrorPage(true);
            setEnableTransition(true);
            /* if(err.response.status ){

            } */

        });

    },[]);

    

   



return (
  <BouncTransition 
  isEnabled={enableTransition}
  render={errorPage?  <Result
    status="500"
    title="Pokemon no encontrado"
    subTitle="El contenido que has solicidato no esta disponible"
    extra={<Button type="primary" onClick={evt=>history.push("/logged/home")}   >Ir al listado</Button>}
  /> :  <div className='detail-view' >
    <Container>
  
        <Row align="center" >
            <Col sm={12} xs={12} md={3} >
            </Col>
            <Col sm={12} xs={12} md={6} >
                <Card className='custom-card-detail' >
                   <div className='head-card-detail' >
                    
                   
                
                   </div>
                    <Row  >
                        <Col sm={12} xs={12} md={12} >
                          <div className='detail-image' >
                              <img src={pokemonData.sprites?  pokemonData.sprites.other['official-artwork'].front_default:'' } className='img-defailt-big' /> 
                          </div>
                        </Col>
                        <Col  sm={12} xs={12} md={12} >
                            <div className='tag-canon-content' >
                                <Tag icon={<StarOutlined />} color={pokemonData.is_default?'green':'red'} >{pokemonData.is_default? 'Es canon':"No es canon" }</Tag>
                            </div>
                            
                        </Col>
                        
                        <Col sm={12} xs={12} md={12} >
                            <Typography.Title level={3} >{pokemonData.name}</Typography.Title>
                        </Col>
                        <Col sm={12} xs={12} md={12} >
  
                            <div className='content-section-detail' >
                                <Row >
                                    <Col sm={12} xs={12} md={12} >
                                        <Typography.Title level={5} >Descripcion</Typography.Title>
                                    </Col>
                                    <Col sm={12} xs={12} md={4} >
                                        <Typography.Text> P. Exp: {pokemonData.base_experience}</Typography.Text>
                                    </Col>
                                    <Col sm={12} xs={12} md={4} >
  
                                        <Typography.Text> Peso: {pokemonData.weight}</Typography.Text>
                                    </Col>
                                    <Col sm={12} xs={12} md={4} >
                                        <Typography.Text> Altura: {pokemonData.height}</Typography.Text>
                                    </Col>
                                </Row>
                            </div>
  
                        </Col>
                        <Col sm={12} xs={12} md={12} >
  
                            <div className='content-section-detail' >
                                <Row >
                                    <Col sm={12} xs={12} md={12} >
                                        
                                        <List
                                        header={<Typography.Title level={5} >Habilidades</Typography.Title>}
                                        dataSource={pokemonData.abilities?pokemonData.abilities :[]}
                                        renderItem={item => (
                                          <List.Item>
                                            <Typography.Text >{item.ability.name}</Typography.Text>
                                          </List.Item>
                                        )}
                                        />
  
                                       
                                    </Col>
                                   
                                </Row>
                            </div>
  
                        </Col>
                         <Col sm={12} xs={12} md={12} >
  
                            <div className='content-section-detail' >
                                <Row >
                                    <Col sm={12} xs={12} md={12} >
                                        
                                        <List
                                        header={<Typography.Title level={5} >Movimientos</Typography.Title>}
                                        dataSource={pokemonData.moves?pokemonData.moves.splice(0,5) :[]}
                                        renderItem={item => (
                                          <List.Item>
                                            <Typography.Text >{item.move.name}</Typography.Text>
                                          </List.Item>
                                        )}
                                        />
  
                                       
                                    </Col>
                                   
                                </Row>
                            </div>
  
                        </Col>
  
                    </Row>
                   
                </Card>
            </Col>
            <Col sm={12} xs={12} md={3} >
            </Col>
        </Row>
  
  
  
    </Container>
  </div>
  }
  />

)
    

}