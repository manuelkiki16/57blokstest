import React,{useState,useEffect} from "react";
import {notification} from "antd";
import Header from "../components/template/Header";
import {Container,Row,Col} from "react-grid-system";
import PokemonCard from "../components/PokemonCard";
import {GetPokemonList,GetPokemonDetail} from "../network/Pokemons";
import {SetDataList,AddFavorite,RemoveFavorite as RemoveFavoriteRed } from "../redux/actions";
import {useDispatch,useSelector} from "react-redux";
import {LIMIT_POKEMON_QUERY} from "../utils/constants";
import {useHistory} from "react-router-dom";
import {IsInFavorite} from "../utils/global-methods";
import { FadeTransition} from "../components/animations/CustomAnimate";

/**
 * Bad practices bat is more fast than react state
 */

let  dataSet = [];
let next_url = '';
export default function Home(props){


        let dispatch = useDispatch();
        let history = useHistory();
        let [filterText,setFilterText] = useState("");
        let [enableTransition,setEnableTransition] = useState(false);
       
        let [listLoading,setListLoading] = useState(false);
        
        let dataList = useSelector(state=>state.dataList);
       
        let favoriteList =  useSelector(state=>state.favorites);
        

        
        useEffect(() => {
            return () => {
                dataSet = [];
                next_url="";
            }
        }, [])




    const bodyListener = (evt)=>{
        let item_marked = document.querySelector(".item-marked");
        let offsetHeight = document.body.getBoundingClientRect().height;
        //let scrollTop = window.scrollY;
        if( item_marked ){

           let heigth = item_marked.getBoundingClientRect().height;
          let Yposition = item_marked.getBoundingClientRect().y;
        //  let Topposition = item_marked.getBoundingClientRect().top;
          let DiffSesion = (offsetHeight - Yposition);

          if( DiffSesion < heigth && DiffSesion >0 ){
              item_marked.classList.remove("item-marked");
              GetData();
          }

         // console.log({offsetHeight,heigth,Yposition,Topposition,scrollTop,DiffSesion});
            

        }
       
    }

    function GetData(first_time = false){
        setListLoading(true);
         ( next_url? GetPokemonList(next_url):GetPokemonList() ).then(async result=>{
           

             let newContent = [];

             await Promise.all(result.data.results.map( async ele=>{

                  let {data} = await GetPokemonDetail(ele.name);
                 newContent.push(data);


             }));
             if( result.data.next && newContent.length == LIMIT_POKEMON_QUERY ){
                
                next_url = result.data.next;
               
                let percent = Math.round(  newContent.length * 0.60 );
               
                newContent[percent].marked = true;
                let newArrayContact = [...dataSet].map(ele=>{
                    ele.marked=false;
                   
                   
                    return ele;
                }).concat(newContent).map( ele=>{
                    ele.isFavorite = IsInFavorite(ele.id,favoriteList);
                    return ele;
                } );
                
                dataSet = newArrayContact;
                //dispatch(SetMasterDataList( newArrayContact ));
                dispatch(SetDataList(newArrayContact));
            }else{
                let newArrayContact = [...dataSet].map(ele=>{
                    ele.marked=false;
                    ele.isFavorite = IsInFavorite(ele.id,favoriteList);
                   
                    return ele;
                }).concat(newContent).map( ele=>{
                    ele.isFavorite = IsInFavorite(ele.id,favoriteList);
                    return ele;
                } );
                dataSet = newArrayContact;
                //dispatch(SetMasterDataList( newArrayContact ));
                dispatch(SetDataList(newArrayContact));
            }
            setListLoading(false);
            if(first_time){
                setEnableTransition(true);
            }
             





             
         

             

         }).catch(error=>{
            notification.error({
                message:"Error",
                description:error.message
            });
            
         }).finally(()=>{
            setListLoading(false);
         });

    }

    useEffect(()=>{
      
     
        window.addEventListener("scroll",bodyListener,0);
        GetData(true);

    },[]);

    /**
     * Add filter text in useEffect when text in search change
     */
    useEffect(()=>{

        if( filterText ){

            let newDataSet =  dataSet.filter( ele=>String(ele.name ).toLowerCase().indexOf( String(filterText).toLowerCase() ) != -1 ).map(ele=>{
                ele.marked = false;
                return ele;
            });  

            dispatch(SetDataList(newDataSet));


        }else{
            dispatch(SetDataList(dataSet));
        }


    },[filterText]);


    const AddToFavorite = (pokeData)=>{
        dispatch(AddFavorite(pokeData));
        let newDataList = [...dataList];
        let indexFound = newDataList.findIndex(ele=>ele.id == pokeData.id);
        
        if( indexFound != -1){
            newDataList[indexFound].isFavorite = true;
            dispatch(SetDataList(newDataList));
    
        }
    

    }
    const RemoveFavorite = (pkId)=>{

        dispatch(RemoveFavoriteRed(pkId));
        let newDataList = [...dataList];
        let indexFound = newDataList.findIndex(ele=>ele.id == pkId);
        
        if( indexFound != -1){
            newDataList[indexFound].isFavorite = false;
            dispatch(SetDataList(newDataList));
    
        }
    }


    return (

        <FadeTransition
        isEnabled={enableTransition}
        render={  <div >
           
           
            <Header 
             setFilterText={setFilterText}
            />

            <div className='pokemon-content' >

                <Container>

                    <Row justify="center"  >


                        {
                            dataList.map(ele =>
                                <Col key={ele.id} sm={12} md={3} >
                                    <PokemonCard
                                        go_to_detail ={ ()=>history.push(`/logged/detailPokemon/${ele.id}`) }
                                        add_to_favorite={()=>AddToFavorite(ele)}
                                        remove_to_favorite={()=>RemoveFavorite(ele.id)}
                                        is_in_favorite={ele.isFavorite}
                                        
                                        pokemon_id={ele.id}
                                        pokemon_name={ele.name}
                                        pokemon_image={ele.sprites.other['official-artwork'].front_default}
                                        pokemon_height={ele.height}
                                        pokemon_weight={ele.weight}
                                        pokemon_exp={ele.base_experience}
                                        marked={ele.marked} />
                                </Col>
                            )
                        }





                    </Row>



                </Container>



            </div>
        </div>
}
        />
      
    )

}
