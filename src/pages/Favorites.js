import React from "react";
import {Typography} from "antd";
import {Container,Row,Col} from "react-grid-system";
import FavoriteItem from "../components/FavoriteItem";
import {useSelector,useDispatch} from "react-redux";
import {RemoveFavorite} from "../redux/actions";
import {useHistory} from "react-router-dom";

 function Favorites(props){


    let  favoritesList = useSelector( state=>state.favorites );
    let history = useHistory();
    let dispatch = useDispatch();
   
    const DropFavoriteItem = (itemId)=>{

        dispatch(RemoveFavorite(itemId));

    }
    const goToDetail = (itemId)=>{

        history.push("/logged/detailPokemon/"+itemId);

    }

    return (
   <div className='favorites-content' >
   
   <Container  >
   <Row  >
       <Col sm={12} xs={12} md={3} >
        </Col>
        <Col sm={12} xs={12} md={6} >

               
                    <div className='custom-list-fab' >
                        <div>
                            <Typography.Title level={3} >Listado de favoritos</Typography.Title>
                        </div>
                        {
                            favoritesList.map( ele=><FavoriteItem 
                                key={ele.id}
                                favorite_name={ele.name}
                                go_to_detail={()=>goToDetail(ele.id)}
                                drop_favorite={()=>DropFavoriteItem(ele.id)}
                                favorite_ext={ele.base_experience}
                                favorite_image={ele.sprites? ele.sprites.other['official-artwork'].front_default:'' }
                                /> )
                        }
                      
                        
                        
                       

                    </div>
               

        </Col>
       <Col sm={12} xs={12} md={3} >
        </Col>
   </Row>
   
 </Container>
   </div>
    )

}


export default Favorites;