import React from "react"; 
import logo from './logo.svg';

import {
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import RouteConfig from "./routes/RouteConfig";
import {useSelector} from "react-redux";

import { Result, Button } from 'antd';
import {useHistory} from "react-router-dom";
import {useSesionValidator} from "./hooks/useSesionValidator";


function App() {



  return (

     <Switch>
         {
           RouteConfig.map( (ele)=>
           <Route 
             key={ele.name}
             exact={ele.exact}
             path={ele.path}
             render={ props=>
            <ele.component {...props} routes={ele.routes}  />
            }
           />
           )
         }
        
         
     </Switch>


  );
}

export default App;
