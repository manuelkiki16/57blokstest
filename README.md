#  57block test

 57block test

 ## Principales Librerias usadas
 
 [React](https://es.reactjs.org/)  
 [react-router-dom](https://reactrouter.com/web/guides/quick-start)  
 [react-transition-group](https://www.npmjs.com/package/react-transition-group) 
 [Ant-design](https://ant.design/docs/react/introduce)  
 [Redux](https://es.redux.js.org/)  
  

## Correr el proyecto en local  🚀

_Paso a paso para correr el proyecto en local _

#### PASO 1 Descargar el proyecto
``` 
git clone git@gitlab.com:manuelkiki16/57blokstest.git

```

#### PASO 2 Instalar las dependencias

_Ve a la carpeta creada por git._
``` 
cd  57blokstest

```
_Instala las dependencias con npm._
``` 
npm install 

```
#### PASO 3 Ejecutar el proyecto
_Dentro de la carpeta raiz ejecuta el siguente comando._
``` 
npm start

```

# Prueba adicional 
_Dentro del proyecto encontraran una carpeta llamada canvas-test que contiene un .html
el cual es la segunda prueba 
._






 por [ManuelAr-github](https://github.com/manuelARuiz9712) 😊
 [manuelkiki16-gitlab](https://gitlab.com/manuelkiki16) 